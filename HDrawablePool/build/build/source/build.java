import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HColorPool; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {




HColorPool colors;
HDrawablePool pool;

public void setup() {
  
  H.init(this).background(0xff181818);
  

  colors = new HColorPool()
    .add(0xffffffff)
    .add(0xfff7f7f7)
    .add(0xffececec)
    .add(0xff333333, 3)
    .add(0xff0095a8, 2)
    .add(0xff00616f, 2)
    .add(0xffff3300, 2)
    .add(0xffff6600, 2)
  ;

  pool = new HDrawablePool(50);
  pool.autoAddToStage()
    .add(new HShape("mongo1.svg"))
    .add(new HShape("mongo2.svg"))
    .add(new HShape("mongo3.svg"))
    .add(new HShape("mongo4.svg"))
    .add(new HShape("mongo5.svg"))
    .add(new HShape("mongo6.svg"))
    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .strokeJoin(ROUND)
              .strokeCap(ROUND)
              .strokeWeight(1)
              .stroke(0xff000000)
              .size((int)random(150, 400))
              .rotate((int)random(360))
              .loc((int)random(width), (int)random(height))
              .anchorAt(H.CENTER)
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();

  H.drawStage();
}
  public void settings() {  size(1280, 720);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
