import hype.*;
import hype.extended.colorist.HColorField;

HColorField colors;

void setup() {
  size(1280, 720);
  H.init(this).background(#181818);
  smooth();

  colors = new HColorField(width, height)
    .addPoint(0, height/2, #FF3300, .7)
    .addPoint(width, height/2, #0095a8, .7)
    .fillOnly()
  ;

  for(int i = 0; i < 150; i++) {
    HRect d = new HRect();
    d
      .rounding(((int)random(-100, -10)))
      .noStroke()
      .fill(#000000)
      .size((int)random(25, 125), (int)random(25, 125))
      .rotate((int)random(360))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    colors.applyColor(d);
    H.add(d);
  }

  H.drawStage();
}
