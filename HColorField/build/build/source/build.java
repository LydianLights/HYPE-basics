import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HColorField; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {




HColorField colors;

public void setup() {
  
  H.init(this).background(0xff181818);
  

  colors = new HColorField(width, height)
    .addPoint(0, height/2, 0xffFF3300, .7f)
    .addPoint(width, height/2, 0xff0095a8, .7f)
    .fillOnly()
  ;

  for(int i = 0; i < 150; i++) {
    HRect d = new HRect();
    d
      .rounding(((int)random(-100, -10)))
      .noStroke()
      .fill(0xff000000)
      .size((int)random(25, 125), (int)random(25, 125))
      .rotate((int)random(360))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    colors.applyColor(d);
    H.add(d);
  }

  H.drawStage();
}
  public void settings() {  size(1280, 720);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
