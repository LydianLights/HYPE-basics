import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HPixelColorist; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {




HPixelColorist colors;
HPixelColorist inverseColors;

public void setup() {
  
  H.init(this).background(0xff181818);
  

  H.add(new HImage("toriel.png"));

  colors = new HPixelColorist("toriel.png").fillAndStroke();
  inverseColors = new HPixelColorist("toriel_invert.png").fillAndStroke();

  for(int i = 0; i < 150; i++) {
    int size = (int)random(25, 100);
    int orientation = 25;
    HRect d = new HRect();
    d
      .rounding(((int)random(-100, -10)))
      .strokeWeight(1)
      .alpha(150)
      .size(size, size + (int)random(0, 80))
      .rotate(orientation + (int)random(-5, 5))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    // colors.applyColor(d);
    inverseColors.applyColor(d);
    H.add(d);
  }

  H.drawStage();
  noLoop();
}

public void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

public void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
  public void settings() {  size(1280, 720);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
