import hype.*;

void setup() {
  size(1280, 720);
  H.init(this).background(#181818);
  smooth();

  for(int i = 0; i < 150; i++) {
    HRect d = new HRect();
    d
      .rounding(((int)random(-100, -10)))
      .strokeWeight(1)
      .stroke(#ff3300)
      .fill(#111111, 64)
      .size((int)random(25, 125), (int)random(25, 125))
      .rotate((int)random(360))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    H.add(d);
  }

  H.drawStage();
  noLoop();
}

void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
