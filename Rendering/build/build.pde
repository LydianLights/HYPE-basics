import hype.*;
import hype.extended.colorist.HColorPool;
import hype.extended.layout.HGridLayout;
import processing.pdf.*;

int gridSize = 50;
int rows = 15;
int cols = 25;
HColorPool colors;
HDrawablePool pool;

void settings() {
  size(gridSize * cols, gridSize * rows);
}

void setup() {
  H.init(this).background(#202020);
  smooth();
  colors = new HColorPool()
    .add(#ffffff)
    .add(#f7f7f7)
    .add(#ececec)
    .add(#333333, 3)
    .add(#0095a8, 2)
    .add(#00616f, 2)
    .add(#ff3300, 2)
    .add(#ff6600, 2)
  ;
  pool = new HDrawablePool(rows * cols);
  pool.autoAddToStage()
    .add(new HShape("svg1.svg"))
    .add(new HShape("svg2.svg"))
    // .add(new HShape("svg3.svg"))
    // .add(new HShape("svg4.svg"))
    // .add(new HShape("svg5.svg"))
    // .add(new HShape("svg6.svg"))
    .layout(
      new HGridLayout()
        .startX(gridSize / 2)
        .startY(gridSize / 2)
        .spacing(gridSize, gridSize)
        .cols(cols)
    )
    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .strokeJoin(ROUND)
              .strokeCap(ROUND)
              .strokeWeight(0)
              .stroke(#000000)
              .anchorAt(H.CENTER)
              .rotate((int)random(4) * 90)
              .size(50)
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();
    noLoop();
}

void draw() {
  H.drawStage();
}

void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

// vector
void saveVector() {
  PGraphics tmp = null;
  tmp = beginRecord(PDF, "render/render.pdf");
    if (tmp == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(tmp, false, 1);
    }
  endRecord();
}

// raster image
void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
